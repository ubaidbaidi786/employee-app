import React from "react";
import './newemployeepopup.css';
import * as Icon from 'react-feather';
import {Formik, Form, Field, ErrorMessage} from 'formik'

function editEmployeeForm({schema, formSubmit, temp, toggleEditEmployeePopup, handleUpdateEmployee}){
    console.log(temp);
    return(
        <div id="formPopup">

            <Formik
            initialValues={{myFirstName:temp.fname, myLastName:temp.lname, myCnicNo:temp.cnic, myPhoneNo:temp.pnumber, myCity:temp.address, myProvince:temp.province, myGender:temp.gender, myStatus:temp.status, myId:temp.id}}
            validationSchema={schema}
            onSubmit={(values,{ resetForm, setErrors, setStatus, setSubmitting })=>{handleUpdateEmployee(values);resetForm()}}
            >
            {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values})=>
            true?(
            <>

            <div id='formHeader'>
                <div id="formTitle"><b>Update Employee From</b></div>
                <div id="formX"><Icon.X className="X" onClick={()=>toggleEditEmployeePopup(null)} /></div>
            </div>
            <Form>
            <div id="formContent">
                <div className="flexItem">
                    <div className="left"><label  id="lblFirstName" for="firstName">First Name:</label></div>
                    <div className="right">
                        <Field name="myFirstName" value={values.myFirstName} className="textField" type="text" id="firstName"/>
                        <ErrorMessage component="div" name="myFirstName" className="error"/>
                        <br /></div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblLastName" for="lastName">Last Name:</label></div>
                    <div className="right">
                        <Field name="myLastName" value={values.myLastName} className="textField" type="text" id="lastName"/>
                        <ErrorMessage component="div" name="myLastName" className="error" />
                        <br /></div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblCnic" for="cnicNo" >CNIC:</label></div>
                    <div className="right">
                        <Field name="myCnicNo" value={values.myCnicNo} className="textField" type="text" id="cnicNo" maxLength='15'/>
                        <ErrorMessage component="div" name="myCnicNo" className="error" />
                    </div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblPhoneNo" for="phoneNo">Phone No:</label></div>
                    <div className="right">
                        <Field name="myPhoneNo" value={values.myPhoneNo} className="textField" type="text" id="phoneNo"/>
                        <ErrorMessage component="div" name="myPhoneNo" className="error" />
                    </div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblCity" for="city">City:</label></div>
                    <div className="right">
                        <Field name="myCity" value={values.myCity} className="textField" type="text" id="city"/>
                        <ErrorMessage component="div" name="myCity" className="error" />
                    <br /></div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblCity" for="province">Province:</label></div>
                    <div className="right">
                        <select id="province" name="myProvince" defaultValue={values.myProvince} onChange={(e)=>{values.myProvince = e.target.value}}>
                            <option>Select province</option>
                            <option>Balochistan</option>
                            <option>KPK</option>
                            <option>Punjab</option>
                            <option>Sindh</option>
                        </select>
                        <ErrorMessage component="div" name="myProvince" className="error" />
                        <br />
                    </div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblGender" for="gender">Gender:</label></div>
                    <div className="right">
                        <i className="btnRadio">
                            <label><input type="radio" name="myGender" defaultChecked={values.myGender=="Male"} onChange={(e)=>{values.myGender="Male"}}/>Male</label>
                            <label><input type="radio" name="myGender" defaultChecked={values.myGender=="Female"} onChange={(e)=>{values.myGender="Female"}}/>Female</label>
                            <label><input type="radio" name="myGender" defaultChecked={values.myGender=="Not Spacified"} onChange={(e)=>{values.myGender="Not Spacified"}}/>Not Spacified</label>
                        </i>
                        <ErrorMessage component="div" name="myGender" className="error" /><br />
                    </div>
                </div>
                <div className="flexItem">
                    <div className="left"><label id="lblStatus">Status:</label></div>
                    <div className="right"><div className="flex">
                        <input name="myStatus" type="checkbox" id="status" onChange={(e)=>{values.myStatus=(e.target.checked ? "Married" : "Single")}} defaultChecked={values.myStatus === "Married"} /><label className="status" for="status">Are you married?</label></div>
                        <ErrorMessage component="div" name="myStatus" className="error" /><br /></div>
                </div>
            </div>
            <div id="formFooter">
                <button id="btnSubmit" style={{backgroundColor:"#0cba00",borderColor:"#26a80c"}} type="submit" className="formButton">Update</button>
                <button id="btnCancel" onClick={()=>toggleEditEmployeePopup(null)} className="formButton">Cancel</button>
            </div>
            </Form>

            </>
            ):(<></>)
            }


            </Formik>
        </div>
    );
}

export default editEmployeeForm;