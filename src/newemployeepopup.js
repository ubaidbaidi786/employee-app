import React from "react";
import './newemployeepopup.css';
import * as Icon from 'react-feather';
import {Formik, Form, Field, ErrorMessage} from 'formik'
import InputMask from 'react-input-mask'

function newEmployeeForm({schema, formSubmit, register, handleSubmit, errors, toggleAddEmployeePopup, handleSubmitEmployee}){
    return(
        <div id="formPopup">
            <Formik
            initialValues={{myFirstName:"", myLastName:"", myCnicNo:"", myPhoneNo:"", myCity:"", myProvince:"Select province", myGender:"Male", myStatus:"Single"}}
            validationSchema={schema}
            onSubmit={(values,{ resetForm, setErrors, setStatus, setSubmitting })=>{handleSubmitEmployee(values);resetForm()}}
            >
            {({errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values})=>
            true?(
            <>



            <div id='formHeader'>
                <div id="formTitle"><b>New Employee From</b></div>
                <div id="formX"><Icon.X className="X" onClick={()=>toggleAddEmployeePopup()} /></div>
            </div>
            <Form>
                <div id="formContent">
                    <div className="flexItem">
                        <div className="left"><label  id="lblFirstName" for="firstName">First Name:</label></div>
                        <div className="right">
                            <Field name="myFirstName" className="textField" type="text" id="firstName"/>
                            <ErrorMessage component="div" name="myFirstName" className="error" />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblLastName" for="lastName">Last Name:</label></div>
                        <div className="right">
                            <Field name="myLastName" className="textField" type="text" id="lastName"/>
                            <ErrorMessage component="div" name="myLastName" className="error" />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblCnic" for="cnicNo" >CNIC:</label></div>
                        <div className="right">
                            <InputMask mask="99999-9999999-9" onBlur={handleBlur} value={values.myCnicNo} disabled={false} maskChar="" onChange={handleChange}>
                                {()=><Field name="myCnicNo" className="textField" type="text" id="cnicNo" maxLength='15'/>}
                            </InputMask>
                            <ErrorMessage component="div" name="myCnicNo" className="error" />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblPhoneNo" for="phoneNo">Phone No:</label></div>
                        <div className="right">
                            <InputMask mask="9999-9999999" value={values.myPhoneNo} disabled={false} maskChar="" onChange={handleChange}>
                                {()=><Field name="myPhoneNo" className="textField" type="text" id="phoneNo"/>}
                            </InputMask>
                            <ErrorMessage component="div" name="myPhoneNo" className="error" />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblCity" for="city">City:</label></div>
                        <div className="right">
                            <Field name="myCity" className="textField" type="text" id="city"/>
                            <ErrorMessage component="div" name="myCity" className="error" />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblCity" for="province">Province:</label></div>
                        <div className="right">
                            <div>
                                <select id="province" name="myProvince" onChange={(e)=>{values.myProvince = e.target.value}}>
                                    <option className="provinceOption">Select province</option>
                                    <option className="provinceOption">Balochistan</option>
                                    <option className="provinceOption">KPK</option>
                                    <option className="provinceOption">Punjab</option>
                                    <option className="provinceOption">Sindh</option>
                                </select>
                                <ErrorMessage component="div" name="myProvince" className="error" />
                            </div>
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblGender" for="gender">Gender:</label></div>
                        <div className="right">
                            <i className="btnRadio">
                                <label><input type="radio" name="myGender" onChange={(e)=>{values.myGender=e.target.value}} defaultChecked="true" value="Male"/>Male</label>
                                <label><input type="radio" name="myGender" onChange={(e)=>{values.myGender=e.target.value}} value="Female"/>Female</label>
                                <label><input type="radio" name="myGender" onChange={(e)=>{values.myGender=e.target.value}} value="Not Spacified"/>Not Spacified</label>
                            </i>
                            <ErrorMessage component="div" name="myGender" className="error" />
                            <br />
                        </div>
                    </div>
                    <div className="flexItem">
                        <div className="left"><label id="lblStatus">Status:</label></div>
                        <div className="right">
                            <div className="flex">
                            <input type="checkbox" id="status" name="myStatus" onChange={(e)=>{values.myStatus=(e.target.checked ? "Married" : "Single")}} defaultChecked={values.myStatus === "Married"} value="Married"/>
                            <label className="status" for="status">Are you married?</label>
                            </div><ErrorMessage component="div" name="myStatus" className="error" />
                            <br /></div>
                        
                    </div>
                </div>
                <div id="formFooter">
                    <button type="submit" id="btnSubmit" className="formButton">Submit</button>
                    <button id="btnCancel" onClick={()=>toggleAddEmployeePopup()} className="formButton">Cancel</button>
                </div>
            </Form>

            </>
            ):(<></>)
            }


            </Formik>
        </div>
    );
}

export default newEmployeeForm;

//<button id="btnSubmit" onClick={()=>handleSubmitEmployee()} className="formButton">Submit</button>
//<button id="btnCancel" onClick={()=>toggleAddEmployeePopup()} className="formButton">Cancel</button>
