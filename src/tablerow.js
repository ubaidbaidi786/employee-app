import React from 'react'
import * as Icon from 'react-feather';
function tablerow({employee, toggleEditEmployeePopup, handleDeleteEmployee}){
    return(
        <tr>
        <td>{employee.id}</td>
        <td>{employee.fname+" "+employee.lname}</td>
        <td>{employee.cnic}</td>
        <td>{employee.gender}</td>
        <td>{employee.pnumber}</td>
        <td>{employee.province}</td>
        <td>{employee.address}</td>
        <td>{employee.status}</td>
        <td className="tdButton">
          <button id='btnEditEmployee' onClick={()=>toggleEditEmployeePopup(employee.id)}><Icon.Edit3 size={20} /></button>
          <button id='btnDeleteEmployee' onClick={()=>handleDeleteEmployee(employee.id)}><Icon.Delete size={20} /></button>
        </td>
      </tr>
    );
}

export default tablerow;