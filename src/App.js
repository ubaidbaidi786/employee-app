
import './App.css';
import React,{useState, useEffect} from 'react';
import Row from './tablerow';
import AddForm from './newemployeepopup';
import axios from 'axios';
import EditEmployeeForm from './editemployeepopup';
import swal from 'sweetalert'
import * as yup from 'yup';
import {useForm} from 'react-hook-form'
import {yupResolver} from "@hookform/resolvers/yup"

const schema = yup.object().shape({
  myFirstName: yup.string().min(3, "First Name Must be at least 3 characters").max(25, "First Name Must be at most 25 characters").matches(/^[a-zA-Z ]+$/, "Only alphabets allowed as Name").required("First name should be required please!"),
  myLastName: yup.string().min(0).max(25, "Last Name Must be at most 25 characters").matches(/[a-zA-Z]||''+$/, "Only alphabets allowed as Name"),
  myPhoneNo: yup.string().min(12).max(12).required("Phone number should be required please!").matches(/[0-9]{4}[-]{1}[0-9]{7}/),
  myCnicNo: yup.string().min(15).max(15).required("CNIC number is required please").matches(/[0-9]{5}[-]{1}[0-9]{7}[-]{1}[0-9]{1}/,'Must Contains Only Numbers'),
  myCity: yup.string().matches(/[a-z A-Z]||''+$/),
  myProvince: yup.string().required("Select Your Province!").notOneOf(["Select province"]),
  myGender: yup.string(),
  myStatus: yup.string()
});

function App() {
  const {register, handleSubmit, formState:{errors}, reset} = useForm({
    resolver: yupResolver(schema)
  });

  const url = "https://localhost:44390/api/";
  var [myEmployee,setMyEmployee] = useState([{id:"1",fname:"Ali",lname:"haider",cnic:"2132-213213-1",gender:"Male",province:"KPK",status:"Single",address:"Havelian",pnumber:"-931-29"}]);
  const [firstName,setFirstName] = useState("");
  var [lastName, setLastName] =  useState(" ");
  var [phoneNo,setPhoneNo] = useState("");
  var [city, setCity] = useState(" ");
  var [cnicNo,setCnicNo] = useState("");
  var [province, setProvince] = useState("Select province");
  var [gender, setGender] = useState("Male");
  var [status, setStatus] = useState("Single");
  var [addEmployeePopup, setAddEmployeePopup] = useState(false);
  var [editEmployeePopup, setEditEmployeePopup] = useState(false);
  var [id, setId] = useState("");
  var [valid, setValid] = useState({fname:false, lname:false, cnic:false, gender:false, province:false, status:false, address:false, pnumber:false});
  var [temp, setTemp] = useState(null);
  var getEmployee = async ()=>{
    var temp =await axios.get(url+"Employee/GetAllEmployee");
    //console.log(temp);
    //console.log(temp.data.result);
    setMyEmployee(temp.data.result);
    console.log(temp.data.result);
  }
  useEffect(()=>{
      getEmployee();
    },[]);

  const resetValid = ()=>{
    setValid({fname:false, lname:false, cnic:false, gender:false, province:false, status:false, address:false, pnumber:false});
  }
  const handleFirstName = (e) =>{
    console.log(e.target.value)
    var regex = /^[ a-zA-Z_]+$/;
    if(regex.test(e.target.value) || e.target.value==''){
      if(e.target.value.length<25){
        setFirstName(e.target.value);   
      }
      if(e.target.value.length == 0){
        var err = valid;
        err.fname = true;
        setValid(err);
      }
      else{
        var err = valid;
        err.fname = false;
        setValid(err);
      }
    }
    
  }
  const handleLastName = (e) =>{
    var regex = /^[ a-zA-Z_]+$/;
    if(regex.test(e.target.value) || e.target.value==''){
      if(e.target.value.length<11){
        setLastName(e.target.value);
      }
      if(e.target.value.length == 0){
        var err = valid;
        err.lname = true;
        setValid(err);
      }
      else{
        var err = valid;
        err.lname = false;
        setValid(err);
      }
      if(e.target.value == "")
      {
        setLastName(" ");
      }
    }
  }
  const handlePhoneNo = (e) =>{
    var regex = /^[0-9]+$/;
    if((e.target.value.length < 13) && (regex.test(e.nativeEvent.data) || e.target.value=='' || e.nativeEvent.data == null)){
      if(e.nativeEvent.data == null){
        setPhoneNo(e.target.value);
        var err = valid;
        err.pnumber = true;
        setValid(err)
      }
      else if(e.target.value.length == 5){
        setPhoneNo(phoneNo+'-'+e.nativeEvent.data);
        var err = valid;
        err.pnumber = true;
        setValid(err)
      }
      else{
        setPhoneNo(e.target.value);
        var err = valid;
        err.pnumber = true;
        setValid(err)
      }
      if(e.target.value.length == 12){
        var err = valid;
        err.pnumber = false;
        setValid(err)
      }
      
      
    }
  }
  const handleCity = (e) =>{
    var regex = /^[a-zA-Z]+$/;
    console.log("City")
    if(regex.test(e.nativeEvent.data) || e.target.value==''){
      console.log("City 21")
      if(e.target.value.length < 16){
        setCity(e.target.value);
      }
    }
  }
  const handleCnicNo = (e) =>{
    var regex = /^[0-9]+$/;
    if((e.target.value.length < 15) && (regex.test(e.nativeEvent.data) || e.target.value=='' || e.nativeEvent.data == null)){
      if(e.nativeEvent.data == null)
      {
        setCnicNo(e.target.value)
        var err = valid;
        err.cnic = true;
        setValid(err);
      }
      else if(e.target.value.length === 6 || e.target.value.length === 14){
        setCnicNo(cnicNo+'-'+e.nativeEvent.data)
        var err = valid;
        err.cnic = true;
        setValid(err);
      }else{
        setCnicNo(e.target.value);
        var err = valid;
        err.cnic = true;
        setValid(err);
      }
      if(e.target.value.length>13){
        var err = valid;
        err.cnic = false;
        setValid(err);
      }
    }
  }
  const handleProvince = (e) =>{
    
    setProvince(e);
    console.log(e);
    if(e == "Select province"){
      var err = valid;
        err.province = true;
        setValid(err);
    }
    else{
      var err = valid;
        err.province = false;
        setValid(err);
    }
  }
  const handleGender = (e)=>{
    setGender(e);
  }
  const handleStatus = (e)=>{
    if(e){
      setStatus("Married");
    }
    else{
      setStatus("Single");
    }
  }
  const toggleAddEmployeePopup = () =>{
    setAddEmployeePopup(!addEmployeePopup)
    console.log(addEmployeePopup);
    setFirstName("");
    setLastName(" ");
    setPhoneNo("");
    setCnicNo("");
    setCity(" ");
    setGender("Male");
    resetValid();
    setEditEmployeePopup(false);
  }
  const toggleEditEmployeePopup = async(id) =>{
    
    if(id !== null){
      console.log(id+" Hi! I am going to edit new employee."+editEmployeePopup);
      var temp = await axios.get(url+"Employee/GetEmployeeById?id="+id);
      setTemp(temp.data.result[0]);
      setId(temp.data.result[0].id);
      /*setFirstName(temp.data.result[0].fname);
      setLastName(temp.data.result[0].lname);
      setCity(temp.data.result[0].address);
      setCnicNo(temp.data.result[0].cnic);
      setPhoneNo(temp.data.result[0].pnumber);
      setGender(temp.data.result[0].gender);
      setProvince(temp.data.result[0].province);
      setStatus(temp.data.result[0].status);
      resetValid();*/
      console.log(id);
    }
    else{
      setFirstName("");
      setLastName(" ");
      setPhoneNo("");
      setCnicNo("");
      setCity(" ");
      setGender("Male");
      setProvince("Select province");
    }
    setEditEmployeePopup(!editEmployeePopup);
    setAddEmployeePopup(false);
  }
  const handleDeleteEmployee = async(id) =>{
    
    await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Employee!",
      icon: "warning",
      buttons: ["Cancel","Delete"], //delete button will goes red and manage delete action etc...
      dangerMode: true,
    })
    .then(async(willDelete) => {
      if (willDelete) {
        var temp =await axios.delete(url+"Employee/DeleteEmployee?id="+id+"");
        if(temp.data.code===200){
            swal("Poof! Employee has been deleted!", {
            icon: "success",
          });
          resetValid();
          getEmployee();
        }
        else{
          swal(temp.data.code+"\n"+temp.data.message);
        }
      }
    });

    console.log("Hi! I am going to delete new employee.");
    /*if(window.confirm("Are you sure to delete this") === true){
        var temp =await axios.delete(url+"Employee/DeleteEmployee?id="+id+"");
        if(temp.data.code===200){
          getEmployee();
        }
        else{
          alert(temp.data.code+"\n"+temp.data.message);
        }

    }*/
  }
  const handleSubmitEmployee = async(data) =>{
    
      console.log(data);

      var temp =await axios.post(url+"Employee/AddEmployee?Fname="+data.myFirstName+"&Lname="+data.myLastName+"&Cnic="+data.myCnicNo+"&Address="+data.myCity+"&Pnumber="+data.myPhoneNo+"&Province="+data.myProvince+"&Gender="+data.myGender+"&Status="+data.myStatus);
      if(temp.data.code===200){
        swal("Employee Added Sucsessfuly", {
          icon: "success",
        });
        toggleAddEmployeePopup();
        getEmployee();
      }
      else{
        swal(temp.data.message)
      }
    
  }
  const handleUpdateEmployee = async(data) =>{
    console.log(data)
    
    
      var temp =await axios.put(url+"Employee/UpdateEmployee?Id="+data.myId+"&Fname="+data.myFirstName+"&Lname="+data.myLastName+"&Cnic="+data.myCnicNo+"&Address="+data.myCity+"&Pnumber="+data.myPhoneNo+"&Province="+data.myProvince+"&Gender="+data.myGender+"&Status="+data.myStatus);
      swal("Employee Updated Sucsessfuly", {
        icon: "success",
      });
      getEmployee();
      resetValid();
      setEditEmployeePopup(false);
    
  }
  const formSubmit = (data)=>{
    console.log(data);
    console.log("Hello")
    //console.log(data.myStatus !="false" ? "Married" : "Single");
  }


  return (
    <div id='main'>
      {addEmployeePopup && (<>
        <AddForm 
          schema={schema}
          formSubmit={formSubmit}
          register={register}
          handleSubmit={handleSubmit}
          errors={errors}
          toggleAddEmployeePopup={toggleAddEmployeePopup}
          handleSubmitEmployee={handleSubmitEmployee}
        />
      </>)}
      {
        editEmployeePopup && (<>
          <EditEmployeeForm 
            schema={schema}
            formSubmit={formSubmit}
            temp = {temp}
            toggleEditEmployeePopup={toggleEditEmployeePopup}
            handleUpdateEmployee={handleUpdateEmployee}
          />
        </>)
      }
      
      <div id='inner'>
        <div id='content'>
          <div id='top'>
            <button id='btnAddNewEmployee' onClick={()=>toggleAddEmployeePopup()}>Add Employee</button>
          </div>
          <div id='bottom'>
            <table id='employeeTable'>
              <tbody>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>CNIC</th>
                  <th>Gender</th>
                  <th>Phone No</th>
                  <th>Province</th>
                  <th>City</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
                    {
                      myEmployee.map(employee=>
                        <Row 
                          employee={employee} 
                          toggleEditEmployeePopup={toggleEditEmployeePopup} 
                          handleDeleteEmployee={handleDeleteEmployee} />
                        )
                    }  
                </tbody>      
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
export default App;
